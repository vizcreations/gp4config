/**
* @general functions source for gp4config
* @author VizCreations
*
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "gfx.h"
#include "gnr.h"

void strip_newline(char *str, int len) {
	int i = 0;
	for(i=0; i<len; i++) {
		if(str[i] == '\n') {
			str[i] = '\0';
			break;
		}
	}
}

void strip_tab(char *str, int len) {
	int i = 0;
	for(i=0; i<len; i++) {
		if(str[i] == '\t') {
			str[i] = '\0';
			break;
		}
	}
}
