# Makefile for gp4 gfx configurator linux build

SRC_CIL = fio.c gnr.c gfx.c
OBJ_CIL = fio.o gnr.o gfx.o

CIL_INCLUDES = -I.
CIL_LIBS = -L.

all: lib_cil compile

lib_cil:
	gcc -c $(SRC_CIL)
	ar rcs gfx.a $(OBJ_CIL)
	$(RM) *.o

compile:
	gcc $(CIL_INCLUDES) -o Config main.c gfx.a $(CIL_LIBS) -lc

clean:
	rm Config* *~
