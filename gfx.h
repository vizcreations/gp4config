/**
* @gp4gfx Library header file for gp4 gfx configurator
* @author VizCreations
*
*
*/


/**
* Setting up constants for max size of elements
*
*/
#define MAX_PATH 1000
#define MAX_STRUCT 5000
#define MAX_LEN 200
#define MAX_LINE 255
#define MAX_LINES 100
#define MAX_VAL 20

/* BOOLEAN constants */
#define TRUE 1
#define FALSE 0

/* PRESET constants */
#define MAXSETTING 100003
#define MEDSETTING 100002
#define LOWSETTING 100001

/**
* The configurator object data structure type definition
* @param mixed
* @abstract The struct defines the members which are of different types
*/
typedef struct Configurator {
	char file_name[256];
	char *cur_line;
	char **all_lines;
	int tot_lines;
	int cur_sel;
	FILE *fp;
	int global;
	int global_key;
	char *global_cmt;
	int res_width;
	int res_width_key;
	char *res_width_cmt;
	int res_height;
	int res_height_key;
	char *res_height_cmt;
	int hw_tnl;
	int hw_tnl_key;
	char *hw_tnl_cmt;
	int wnd_mode;
	int wnd_mode_key;
	char *wnd_mode_cmt;
	int vsync;
	int vsync_key;
	char *vsync_cmt;
	int tex_qlty;
	int tex_qlty_key;
	char *tex_qlty_cmt;
	int bump_map;
	int bump_map_key;
	char *bump_map_cmt;
	int mirrors;
	int mirrors_key;
	char *mirrors_cmt;
	int track_map;
	int track_map_key;
	char *track_map_cmt;
	int env_map;
	int env_map_key;
	char *env_map_cmt;
	int tex_fil_qlty;
	int tex_fil_qlty_key;
	char *tex_fil_qlty_cmt;
	int an_fil_qlty;
	int an_fil_qlty_key;
	char *an_fil_qlty_cmt;
	int shdws;
	int shdws_key;
	char *shdws_cmt;
	int shdw_type;
	int shdw_type_key;
	char *shdw_type_cmt;
	int ext_steer;
	int ext_steer_key;
	char *ext_steer_cmt;
	int heat_haze;
	int heat_haze_key;
	char *heat_haze_cmt;

	/** Buffers to hold values */
	char *value;
	char *comment;
} CFG;

void get_val_cmt_(CFG *);
void set_val_cmt_(CFG *);
int int_in_array(int, int arr[], int);
int lengthof(int arr[]);
int get_val_key(int arr[], int, size_t);
int edit_tex_fil_qlty(CFG *, int);
void edit_track_map(CFG *);
void edit_env_map(CFG *);
void edit_tex_qlty(CFG *);
void edit_an_fil_qlty(CFG *);
void edit_shdw_type(CFG *);
void init_vals(CFG *);
void free_vals(CFG *);
