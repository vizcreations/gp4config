/**
* @general functions header with prototypes
* @author VizCreations
*
*/

void strip_newline(char *, int);
void strip_tab(char *str, int);
