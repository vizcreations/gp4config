/**
* @file i/o source file with functions for gp4config
* @author VizCreations
*
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "gfx.h"
#include "fio.h"

void save_file(CFG *cfg) {
	int i = 0;
	//printf("Generating new configuration file from buffer.. ");
	cfg->fp = fopen("f1graphics.cfg", "w");
	if(!cfg->fp) {
		printf("Couldn't write file!\n");
	} else {
		for(i=0; i<cfg->tot_lines; i++) {
			fprintf(cfg->fp, "%s\n", cfg->all_lines[i]);
		}
		fclose(cfg->fp);
		printf("New configuration file created successfully!\n");
	}
}

void show_file_contents(CFG *cfg) {
	char *buff;
	int counter = 0;
	cfg->fp = fopen(cfg->file_name, "r");
	//if(sizeof(buff) > 0) free(buff);
	buff = (char *) malloc(sizeof(char) * MAX_LINE);
	while(fgets(buff, MAX_LINE, cfg->fp)) {
		printf("%s", buff);
		free(buff);
		buff = (char *) malloc(sizeof(char) * MAX_LINE);
		++counter;
	}
	free(buff);
}

void show_cur_val(CFG *cfg) {
	//printf("Change settings as below:\n");
	printf("\nNow editing:%s\n", cfg->comment);
}

int read_file(CFG *cfg) {
	int num;
	FILE *handler;

	cfg->tot_lines = 0;
	//free(cfg->all_lines);
	//free(cfg->cur_line);
	/*memset(cfg->all_lines, 0, sizeof(char) * MAX_LINE * MAX_LINES);
	memset(cfg->cur_line, 0, sizeof(char) * MAX_LINE);*/
	cfg->all_lines = (char **) malloc(sizeof(char) * MAX_LINE * MAX_LINES);
	cfg->cur_line = (char *) malloc(sizeof(char) * MAX_LINE);

	while(fgets(cfg->cur_line, MAX_LINE, cfg->fp)) {
		strip_newline(cfg->cur_line, MAX_LINE);
		if(strlen(cfg->cur_line) > 0) {
			cfg->all_lines[cfg->tot_lines] = (char *) malloc(sizeof(char) * MAX_LINE);
			strcpy(cfg->all_lines[cfg->tot_lines], cfg->cur_line);
			free(cfg->cur_line);
			cfg->cur_line = (char *) malloc(sizeof(char *) * MAX_LINE);
			++cfg->tot_lines;
		}
	}
	//printf("Total: %d\n", cfg->tot_lines);

	fclose(cfg->fp);
	if(cfg->tot_lines == 0) return 0;
	//printf("Completed successfully..\n");

	//printf("Now creating backup file.. ");
	handler = fopen("f1graphics.cfg.backup", "w");
	if(handler) {
		for(num=0; num<cfg->tot_lines; num++) {
			fprintf(handler, "%s\n", cfg->all_lines[num]);
		}
		fclose(handler);
	} else return 0;

	return 1;

	//init_vals(cfg);
}
