About GP4 Graphics Configurator:
================================

This application is an attempt to optimize GP4 graphics from the outside. 
It performs very similarly like in-game in allowing you to change settings.
The program is still in it's first days, so expect it to not be 
extremely user friendly.


How it works:
=============

Just run the program after copying it to your user directory. 
Example: C:\Users\MyName\GP4ConsoleConfig
When doing your settings, make sure you do not go beyond or 
below the settings mentioned in the program.
After making all necessary changes and generating the configuration file, 
copy the newly generated file to your GP4 installation directory.
The program creates a backup file everytime you run it 
(f1graphics.cfg.backup), so in case of any misconfigurations you 
can always copy the original backup to your GP4 installation directory.


What to know:
=============

GP4 gfx configurator is an unofficial program. It is completely free 
and does not give any sort of guarantee 
to the user using it. It is under your own discretion to use the program.
There is no copyright attached, and you can use the program as you like 
and I do not really mind about it.


Community:
==========

The core source code of the program is deployed on git 
for anyone to change - https://bitbucket.org/webauro/gp4config
The reason is only to make you people come up with your own versions.


~Webauro. :-)
