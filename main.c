/**
* @main source file for gp4 gfx configurator
* @author VizCreations
* @output Linux shell
*
*/

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include "gfx.h"
#include "fio.h"
#include "gnr.h"

char *token; // Pulls out the string using delimiter in each setting line
char *buff; // Buffer that holds the current pulled out string from token
char *value; // Value is what is the first token
char *comment; // Comment is second token
int val; // The char * (string) value should be converted to integer for simulation
int i; // The iterator number
//char sel; // Current selection from console
//char *token;

char delimiters[] = " .,;|"; // The bunch of delimiters which could be used

void start_(CFG *cfg) {
	int x, key, val;
	char sel = 'y';
	char *settings[10]; // 10 Max setting with value upto 100 characters
	int values[10]; // 10 max settings for any opton
	int edited = FALSE;
	printf("Initialized values from file to memory successfully..\n");
	printf("Do you want to proceed? (y/n): ");
	scanf("%c", &sel);
	sel = tolower(sel);
	while(sel != 'n') { // One setting after another. Regular procedure
		if(sel == 'y') {
			/** Texture filter quality setting */
			settings[0] = (char *) malloc(sizeof(char) * MAX_LINE);
			settings[0] = "None";
			settings[1] = (char *) malloc(sizeof(char) * MAX_LINE);
			settings[1] = "Point";
			settings[2] = (char *) malloc(sizeof(char) * MAX_LINE);
			settings[2] = "Bilinear";
			settings[3] = (char *) malloc(sizeof(char) * MAX_LINE);
			settings[3] = "Anisotropic";
			settings[4] = (char *) malloc(sizeof(char) * MAX_LINE);
			settings[4] = "Flat Cubic";
			settings[5] = (char *) malloc(sizeof(char) * MAX_LINE);
			settings[5] = "GaussianCubic";

			values[0] = 0;
			values[1] = 1;
			values[2] = 2;
			values[3] = 3;
			values[4] = 4;
			values[5] = 5;
			printf("\nTexture Filter Quality settings:\n");
			printf("================================\n");

			for(x=0; x<6; x++) {
				printf("%d\t=>\t%s\n", values[x], settings[x]);
			}

			cfg->cur_sel = cfg->tex_fil_qlty_key;
			get_val_cmt_(cfg);
			//show_cur_val(cfg);

			key = get_val_key(values, atoi(cfg->value), 6);
			while(edited == FALSE) {
				printf("\n(%s - current) Enter value to change: ", settings[key]);
				scanf("%d", &val);
				edited = edit_tex_fil_qlty(cfg, val);
				if(edited == FALSE) {
					printf("Invalid value entered, try again..\n");
				} else {
					printf("Value edited successfully to %s..\n", settings[val]);
				}
			}

			edited = FALSE;
			/* Track map setting */
			edit_track_map(cfg);

			/* Environment setting */
			edit_env_map(cfg);

			/* Texture quality setting */
			edit_tex_qlty(cfg);

			/* Anisotropic filter quality setting */
			edit_an_fil_qlty(cfg);

			/* Shadow type setting */
			edit_shdw_type(cfg);
			printf("\nDo you want to save settings to file? (y/n): ");
			getchar();
			scanf("%c", &sel);
			if(tolower(sel) == 'y') {
				save_file(cfg);
			}
			getchar();
			//start_(cfg);
			/*} else if(sel == 'f' || sel == 'F') {
				show_file_contents(cfg);
				getchar();
				//start_(cfg);
			}*/
			//}
			printf("Do you want to reconfigure? (y/n): ");
			scanf("%c", &sel);
		} else if(sel == 'p') {
			// Show preset selection
		}
	}
}

int main(int argc, char *argv[], char *env[]) {
	CFG cfg;
	printf("\nGP4 graphics configurator v2.0\n");
	//printf("Specify the absolute path for the configuration file: ");
	memset(cfg.file_name, 0, 256);
	//fgets(cfg.file_name, 256, stdin);
	if(argc < 2) {
		printf("Usage: %s <path of f1graphics.cfg>\n", argv[0]);
		return -1;
	}

	if(argc > 1) {
		strcpy(cfg.file_name, argv[1]);
	}
	strip_newline(cfg.file_name, 256);

	if(strlen(cfg.file_name) > 0) {
		cfg.fp = fopen(cfg.file_name, "r");
		if(cfg.fp) {
			printf("Loading from location: %s\n\n", cfg.file_name);
			printf("Now reading contents of file..\n");
			sleep(1);
			if(!read_file(&cfg)) {
				printf("Error reading file!\n");
				free_vals(&cfg);
				free(cfg.cur_line);
				free(cfg.all_lines);
				return -1;
			}
			printf("Created backup file successfully..\n");
			init_vals(&cfg);

			/** Start program */
			start_(&cfg);
		} else {
			printf("Error reading file!\n");
		}
	} else {
		strcpy(cfg.file_name, "C:\\Program Files\\Infogrames\\Grand Prix 4\\f1graphics.cfg");
		cfg.fp = fopen(cfg.file_name, "r");
		if(!cfg.fp) {
			strcpy(cfg.file_name, "C:\\Program Files (x86)\\Infogrames\\Grand Prix 4\\f1graphics.cfg");
			cfg.fp = fopen(cfg.file_name, "r");
			if(!cfg.fp) {
				printf("Error reading file!\n");
				return -1;
			}
		}
		printf("Loading from default location: %s\n", cfg.file_name);
		printf("Now reading contents of file..\n");
		sleep(1);
		if(!read_file(&cfg)) {
			printf("Error reading file!\n");
			return -1;
		}
		printf("Created backup file successfully..\n");
		init_vals(&cfg);
		start_(&cfg);
	}

	free_vals(&cfg);
	free(cfg.cur_line);
	free(cfg.all_lines);
	cfg.tot_lines = 0;

	if(sizeof(buff) > 0) free(buff);
	if(sizeof(value) > 0) free(value);
	if(sizeof(comment) > 0) free(comment);
	
	return 0;
}

