/**
* @gp4gfx source functions for gp4config
* @author VizCreations
*
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "gfx.h"

void get_val_cmt_(CFG *cfg) {
	char *buff;
	char *token;
	if(sizeof(cfg->value) > 0) free(cfg->value);
	if(sizeof(cfg->comment) > 0) free(cfg->comment);

	buff = (char *) malloc(sizeof(char) * MAX_LINE);
	cfg->value = (char *) malloc(sizeof(char) * MAX_VAL);
	cfg->comment = (char *) malloc(sizeof(char) * MAX_LINE);

	strcpy(buff, cfg->all_lines[cfg->cur_sel]);
	token = strtok(buff, ";");

	if(token != NULL) {
		//value[0] = '\0';
		strcpy(cfg->value, token);
		strip_tab(cfg->value, MAX_VAL);
	}

	//token = strtok(NULL, ";");
	token = strtok(NULL, "\n"); // The previous program cut the comment wherever there was ';' character which is wrong as many comments have ';' character in the middle

	if(token != NULL) {
		//comment[0] = '\0';
		strcpy(cfg->comment, token);
		strip_newline(cfg->comment, MAX_LINE);
	}

}

void set_val_cmt_(CFG *cfg) {
	int last;
	int val;

	free(cfg->all_lines[cfg->cur_sel]);
	cfg->all_lines[cfg->cur_sel] = (char *) malloc(sizeof(char) * MAX_LINE);
	cfg->all_lines[cfg->cur_sel][0] = '\0';

	free(cfg->value);
	cfg->value = (char *) malloc(sizeof(char) * MAX_VAL);
	//value[0] = '\0';

	sprintf(cfg->value, "%d\t\t\t", val);
	strcat(cfg->all_lines[cfg->cur_sel], cfg->value);
	strcat(cfg->all_lines[cfg->cur_sel], ";");
	strcat(cfg->all_lines[cfg->cur_sel], cfg->comment);

	last = strlen(cfg->all_lines[cfg->cur_sel]) + 1;
	cfg->all_lines[cfg->cur_sel][last] = '\0';
}

int int_in_array(int needle, int haystack[], int length) {
	int i;
	int exists = 0;
	for(i=0; i<length; i++) {
		if(needle == haystack[i]) {
			exists = 1;
			break;
		}
	}
	return exists;
}

int lengthof(int arr[]) {
	return (sizeof(arr) / sizeof(arr[0]));
	/*int arry[6];
	return sizeof(arry);*/
}

int get_val_key(int arr[], int val, size_t size) {
	int k;
	int key = 0;
	for(k=0; k<size; k++) {
		if(arr[k] == val) {
			key = k;
			break;
		}
	}
	return key;
}

int edit_tex_fil_qlty(CFG *cfg, int val) {
	int values[] = { 0, 1, 2, 3, 4, 5 };
	if(int_in_array(val, values, 6) == TRUE) {
		cfg->tex_fil_qlty = val;

		strcpy(cfg->tex_fil_qlty_cmt, cfg->comment);

		set_val_cmt_(cfg);
		return TRUE;
	}
	return FALSE;
}

void edit_track_map(CFG *cfg) {
	int x;
	int key;
	int val;
	int values[3] = { 0, 1, 2 };
	char *settings[] = {
				"Off",
				"Arcade Only",
				"All Non Trackside"
				};
	cfg->cur_sel = cfg->track_map_key;
	get_val_cmt_(cfg);
	//show_cur_val(cfg);

	printf("\nTrack Map settings:\n");
	printf("===================\n");
	for(x=0; x<3; x++) {
		printf("%d\t=>\t%s\n", values[x], settings[x]);
	}

	key = get_val_key(values, atoi(cfg->value), 3);
	printf("\n(%s - current) Enter value to change: ", settings[key]);
	scanf("%d", &val);

	if(int_in_array(val, values, 3) == TRUE) {
		cfg->track_map = val;

		strcpy(cfg->track_map_cmt, cfg->comment);

		set_val_cmt_(cfg);
		key = get_val_key(values, val, 3);
		printf("Value edited successfully to %s..\n", settings[key]);
	} else {
		printf("Invalid value entered, try again..\n");
		edit_track_map(cfg);
	}
}

void edit_env_map(CFG *cfg) {
	int val;
	cfg->cur_sel = cfg->env_map_key;
	get_val_cmt_(cfg);
	//show_cur_val(cfg);

	printf("\nEnvironment Map settings:\n");
	printf("=========================");

	printf("\n(%s - current) Enter value to change (0 or more): ", cfg->value);
	scanf("%d", &val);

	if(val >= 0) {
		cfg->env_map = val;

		strcpy(cfg->env_map_cmt, cfg->comment);

		set_val_cmt_(cfg);
		printf("Value edited successfully to %d..\n", val);
	} else {
		printf("Invalid value entered, try again..\n");
		edit_env_map(cfg);
	}
}

void edit_tex_qlty(CFG *cfg) {
	int x;
	int key;
	int val;
	int values[4] = { 0, 2, 4, 8 };
	char *settings[] = {
				"MAX",
				"HALF",
				"QTR",
				"EIGTH"
				};
	cfg->cur_sel = cfg->tex_qlty_key;
	get_val_cmt_(cfg);
	//show_cur_val(cfg);

	printf("\nTexture Quality settings:\n");
	printf("=========================\n");
	
	for(x=0; x<4; x++) {
		printf("%d\t=>\t%s\n", values[x], settings[x]);
	}

	key = get_val_key(values, atoi(cfg->value), 4);

	printf("\n(%s - current) Enter value to change: ", settings[key]);
	scanf("%d", &val);

	if(int_in_array(val, values, 4) == TRUE) {
		cfg->tex_qlty = val;

		strcpy(cfg->tex_qlty_cmt, cfg->comment);

		set_val_cmt_(cfg);
		key = get_val_key(values, val, 4);
		printf("Value edited successfully to %s..\n", settings[key]);
	} else {
		printf("Invalid value entered, try again..\n");
		edit_tex_qlty(cfg);
	}
}

void edit_an_fil_qlty(CFG *cfg) {
	int x;
	int val;
	int values[8] = { 0, 1, 2, 3, 4, 5, 6, 7 };
	cfg->cur_sel = cfg->an_fil_qlty_key;
	get_val_cmt_(cfg);
	//show_cur_val(cfg);
	printf("\nMax Anisotropic Filter Quality settings:\n");
	printf("========================================");

	printf("\n(%s - current) Enter value to change (0 to 7): ", cfg->value);
	scanf("%d", &val);

	if(int_in_array(val, values, 8) == TRUE) {
		cfg->an_fil_qlty = val;

		strcpy(cfg->an_fil_qlty_cmt, cfg->comment);

		set_val_cmt_(cfg);
		printf("Value edited successfully to %d..\n", val);
	} else {
		printf("Invalid value entered, try again..\n");
		edit_an_fil_qlty(cfg);
	}
}

void edit_shdw_type(CFG *cfg) {
	int x;
	int key;
	int val;
	int values[4] = { 0, 1, 2, 3 };
	char *settings[] = {
				"Off",
				"Static",
				"Composite",
				"Projected"
				};
	cfg->cur_sel = cfg->shdw_type_key;
	get_val_cmt_(cfg);
	//show_cur_val(cfg);
	printf("\nShadow Type settings:\n");
	printf("=====================\n");

	for(x=0; x<4; x++) {
		printf("%d\t=>\t%s\n", values[x], settings[x]);
	}

	key = get_val_key(values, atoi(cfg->value), 4);
	printf("\n(%s - current) Enter value to change: ", settings[key]);
	scanf("%d", &val);

	if(int_in_array(val, values, 4) == TRUE) {
		cfg->shdw_type = val;
		strcpy(cfg->shdw_type_cmt, cfg->comment);

		set_val_cmt_(cfg);
		key = get_val_key(values, val, 4);
		printf("Value edited successfully to %s..\n", settings[key]);
	} else {
		printf("Invalid value entered, try again..\n");
		edit_shdw_type(cfg);
	}
}

void init_vals(CFG *cfg) {
	cfg->tex_fil_qlty_key = 98;
	cfg->tex_fil_qlty_cmt = (char *) malloc(sizeof(char) * MAX_LEN);
	cfg->tex_fil_qlty = 0;
	
	cfg->track_map_key = 52;
	cfg->track_map_cmt = (char *) malloc(sizeof(char) * MAX_LEN);
	cfg->track_map = 0;

	cfg->env_map_key = 64;
	cfg->env_map_cmt = (char *) malloc(sizeof(char) * MAX_LEN);
	cfg->env_map = 0;

	cfg->tex_qlty_key = 23;
	cfg->tex_qlty_cmt = (char *) malloc(sizeof(char) * MAX_LEN);
	cfg->tex_qlty = 0;

	cfg->an_fil_qlty_key = 100;
	cfg->an_fil_qlty_cmt = (char *) malloc(sizeof(char) * MAX_LEN);
	cfg->an_fil_qlty = 0;

	cfg->shdw_type_key = 108;
	cfg->shdw_type_cmt = (char *) malloc(sizeof(char) * MAX_LEN);
	cfg->shdw_type = 0;

	cfg->value = (char *) malloc(sizeof(char) * MAX_LEN);
	cfg->comment = (char *) malloc(sizeof(char) * MAX_LINE);
}

void free_vals(CFG *cfg) {
	free(cfg->tex_fil_qlty_cmt);
	free(cfg->track_map_cmt);
	free(cfg->env_map_cmt);
	free(cfg->tex_qlty_cmt);
	free(cfg->an_fil_qlty_cmt);
	free(cfg->shdw_type_cmt);
}
